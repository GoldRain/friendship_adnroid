package com.friendship.model;

/**
 * Created by HugeRain on 3/10/2017.
 */

public class FriendModel {

    int _id = 0;
    String _name = "";
    String _description = "";
    String _firstName = "";
    String _lastName = "";
    String _photo_url = "";
    String _phonnumber = "";
    String fromPhone="";
    String email="";

  /*  @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FriendModel custom = (FriendModel) o;

        if (!email.equals(custom.email)) {
            return false;
        }
        if (!_phonnumber.equals(custom._phonnumber)) {
            return false;
        }

        return true;
    }
*/
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPlusminus() {
        return plusminus;
    }

    public void setPlusminus(String plusminus) {
        this.plusminus = plusminus;
    }

    String plusminus;

    public int get_id() {
        return _id;
    }

    public String getFromPhone() {
        return fromPhone;
    }

    public void setFromPhone(String fromPhone) {
        this.fromPhone = fromPhone;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_description() {
        return _description;
    }

    public void set_description(String _description) {
        this._description = _description;
    }

    public String get_photo_url() {
        return _photo_url;
    }

    public void set_photo_url(String _photo_url) {
        this._photo_url = _photo_url;
    }

    public String get_firstName() {
        return _firstName;
    }

    public void set_firstName(String _firstName) {
        this._firstName = _firstName;
    }

    public String get_lastName() {
        return _lastName;
    }

    public void set_lastName(String _lastName) {
        this._lastName = _lastName;
    }

    public String get_fullName(){

        return _firstName + _lastName;
    }

    public String get_phonnumber() {
        return _phonnumber;
    }

    public void set_phonnumber(String _phonnumber) {
        this._phonnumber = _phonnumber;
    }
}
