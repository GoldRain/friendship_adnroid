package com.friendship.model;

/**
 * Created by vaibhav on 6/4/17.
 */

public class FriendModelAPI {
     String plusminus;
    int _id = 0;
    String _name = "";
    String _description = "";
    String _firstName = "";
    String _lastName = "";
    String _photo_url = "";
    String _phonnumber = "";
    String fromPhone="";

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    String email="";
    public int get_id() {
        return _id;
    }

    public String get_name() {
        return _name;
    }

    public String get_description() {
        return _description;
    }

    public void set_description(String _description) {
        this._description = _description;
    }

    public String get_firstName() {
        return _firstName;
    }

    public String get_lastName() {
        return _lastName;
    }

    public String get_photo_url() {
        return _photo_url;
    }

    public void set_photo_url(String _photo_url) {
        this._photo_url = _photo_url;
    }

    public String get_phonnumber() {
        return _phonnumber;
    }

    public String getFromPhone() {
        return fromPhone;
    }

    public String getPlusminus() {
        return plusminus;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public void set_firstName(String _firstName) {
        this._firstName = _firstName;
    }

    public void set_lastName(String _lastName) {
        this._lastName = _lastName;
    }

    public void set_phonnumber(String _phonnumber) {
        this._phonnumber = _phonnumber;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public void setFromPhone(String fromPhone) {
        this.fromPhone = fromPhone;
    }

    public void setPlusminus(String plusminus) {
        this.plusminus = plusminus;
    }
}
