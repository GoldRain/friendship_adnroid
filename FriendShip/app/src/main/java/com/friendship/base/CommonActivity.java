package com.friendship.base;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;

import com.friendship.commons.Commons;

import java.util.List;
import java.util.Locale;

public abstract class CommonActivity extends BaseActivity{

    @Override
    public void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);

    }

    public String getLanguage(){

        Locale systemLocale = getResources().getConfiguration().locale;
        String strLanguage = systemLocale.getLanguage();

        return strLanguage;
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        Commons.g_isAppPaused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Commons.g_isAppPaused = false;
        Commons.g_isAppRunning = true;
    }

}
