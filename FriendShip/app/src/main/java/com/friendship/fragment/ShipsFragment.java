package com.friendship.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.friendship.R;
import com.friendship.activity.MainActivity;


public class ShipsFragment extends Fragment {

    Context _context;
    MainActivity _activity;
    View view;
    TextView txv_totitle;

    public ShipsFragment(Context context) {
        // Required empty public constructor

        _context = context;
        _activity = (MainActivity)getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_ships, container, false);

        loadLayout();
        return view;
    }

    private void loadLayout() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity) context;
    }

}
