package com.friendship.preference;

public class PrefConst {


    public static final String PREFKEY_USERID  = "user_id";

    public static final String PREFKEY_USEREMAIL = "email";
    public static final String PREFKEY_XMPPID = "XMPPID";
    public static final String PREFKEY_USERPWD = "password";
    public static final String PREFKEY_ISREGISTERED = "is_registered";
    public static final String PREFKEY_LASTLOGINID = "lastlogin_id";

    public static final String PREFKEY_CURRNETLANG = "current_language";

    public static final String Key_access_token = "access_token";
    public static final String Key_token_type = "token_type";
    public static final String Key_expires_in = "expires_in";
    public static final String Key_scope = "scope";
    public static final String Key_refreshtoken = "refresh_token";

    public static final String Key_firstName = "firstName";
    public static final String Key_lastName = "lastName";

    public static final String Key_login = "login";
    public static final String Key_userId = "userId";
    public static final String Key_password = "password";

    public static final String Key_ImageBase64 = "base64";

    public static final String Key_Is_Registering_page = "Is_Registering_page";

}
