package com.friendship;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.friendship.activity.MainActivity;
import com.friendship.base.CommonActivity;
import com.friendship.commons.Commons;

/**
 * Created by HugeRain on 3/8/2017.
 */

public class RestartActivity extends CommonActivity implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!Commons.g_isAppRunning){

            Intent goMain = new Intent(this, MainActivity.class);
            startActivity(goMain);
        }

        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return true;
    }
    @Override
    public void onClick(View v) {

    }
}
