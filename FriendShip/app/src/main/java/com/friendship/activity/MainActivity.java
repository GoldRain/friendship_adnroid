package com.friendship.activity;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.friendship.R;
import com.friendship.adapter.MainViewPagerAdapter;
import com.friendship.base.CommonActivity;
import com.friendship.preference.PrefConst;
import com.friendship.preference.Preference;

public class MainActivity extends CommonActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener{

    ViewPager viewPager;
    DrawerLayout ui_drawerlayout;
    MainViewPagerAdapter _adapter;
    NavigationView drawer_menu;
    ActionBarDrawerToggle drawerToggle;
    ImageView call_drawer;
    TabLayout tabLayout;
    View header_view;
    TextView txv_title;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Preference.getInstance().put(MainActivity.this, PrefConst.Key_Is_Registering_page, 7);
        Preference.getInstance().ResetScreenValue(MainActivity.this);

        loadLaout();
    }

    private void loadLaout() {

        ui_drawerlayout = (DrawerLayout)findViewById(R.id.drawerlayout);
        drawer_menu = (NavigationView)findViewById(R.id.drawer_menu);
        drawer_menu.setNavigationItemSelectedListener(this);
        header_view = drawer_menu.getHeaderView(0);

        FragmentManager manager = getSupportFragmentManager();
        _adapter = new MainViewPagerAdapter(manager, this);

        viewPager = (ViewPager)findViewById(R.id.viewpager);
        viewPager.setAdapter(_adapter);


        tabLayout = (TabLayout)findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        call_drawer = (ImageView)findViewById(R.id.imv_call_drawer);
        call_drawer.setOnClickListener(this);

        txv_title = (TextView)findViewById(R.id.txv_title);
        txv_title.setText(getString(R.string.search));

        setupNavigationBar();
        drawerToggle.syncState();
        setupTabIcons();

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position==0){
                    txv_title.setText(getString(R.string.search));

                }
                else if(position==1){
                    txv_title.setText(getString(R.string.status));
                }
                else if (position == 2){
                    txv_title.setText(getString(R.string.friends));
                }
                else if (position == 3){
                    txv_title.setText(getString(R.string.ships));
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void setupTabIcons() {

        int[] tabIcons = {R.drawable.select_search, R.drawable.select_status, R.drawable.select_friend, R.drawable.select_boote};

        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);

    }

    private void setupNavigationBar() {

        drawerToggle = new ActionBarDrawerToggle(this, ui_drawerlayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {

                super.onDrawerOpened(drawerView);}

            @Override
            public void onDrawerClosed(View drawerView) {

                super.onDrawerClosed(drawerView);}
        };

        ui_drawerlayout.setDrawerListener(drawerToggle);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_call_drawer:
                ui_drawerlayout.openDrawer(Gravity.LEFT);
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        ui_drawerlayout.closeDrawers();

        switch (id){

            case R.id.nav_item1:
                showToast("clicked item1");
                break;

            case R.id.nav_item2:
                showToast("clicked item2");
                break;

            case R.id.nav_item3:
                showToast("clicked item3");
                break;

            case R.id.nav_item4:
                showToast("clicked item4");
                break;

            case R.id.nav_item5:
                showToast("clicked item5");
                break;

            case R.id.nav_item6:
                showToast("clicked item6");
                break;


        }

        return false;
    }

    @Override
    public void onBackPressed() {

        if (ui_drawerlayout.isDrawerOpen(GravityCompat.START)) {
            ui_drawerlayout.closeDrawer(GravityCompat.START);

        } else {
            onExit();
        }
    }

}
