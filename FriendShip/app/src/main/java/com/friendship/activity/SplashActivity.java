package com.friendship.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.friendship.FriendShipApplication;
import com.friendship.R;
import com.friendship.base.CommonActivity;
import com.friendship.commons.Commons;
import com.friendship.commons.Constants;
import com.friendship.commons.Urls;
import com.friendship.preference.PrefConst;
import com.friendship.preference.Preference;
import com.friendship.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SplashActivity extends CommonActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, StartSetupActivity.class));
                finish();

                overridePendingTransition(0,0);
            }
        }, Constants.SPLASH_TIME);*/
        if (Preference.getInstance().getValue(SplashActivity.this, PrefConst.Key_userId, 0) != 0) {
            if (Preference.getInstance().getValue(SplashActivity.this, PrefConst.Key_Is_Registering_page, 0) == 1) {
                startActivity(new Intent(SplashActivity.this, FirstNameActivity.class));
                finish();
                overridePendingTransition(0, 0);
            } else if (Preference.getInstance().getValue(SplashActivity.this, PrefConst.Key_Is_Registering_page, 0) == 2) {
                startActivity(new Intent(SplashActivity.this, MobileNumberActivity.class));
                finish();  overridePendingTransition(0, 0);

            } else if (Preference.getInstance().getValue(SplashActivity.this, PrefConst.Key_Is_Registering_page, 0) == 3) {
                startActivity(new Intent(SplashActivity.this, MobileNumberActivity.class));
                finish();
                overridePendingTransition(0, 0);
            } else if (Preference.getInstance().getValue(SplashActivity.this, PrefConst.Key_Is_Registering_page, 0) == 4) {
                startActivity(new Intent(SplashActivity.this, UploadImageActivity.class));
                finish();
                overridePendingTransition(0, 0);
            } else if (Preference.getInstance().getValue(SplashActivity.this, PrefConst.Key_Is_Registering_page, 0) == 5) {
                startActivity(new Intent(SplashActivity.this, ContactActivity.class));
                finish();
                overridePendingTransition(0, 0);
            } else if (Preference.getInstance().getValue(SplashActivity.this, PrefConst.Key_Is_Registering_page, 0) == 6) {
                startActivity(new Intent(SplashActivity.this, ContactListActivity.class));
                finish();
                overridePendingTransition(0, 0);
            }else {



                dologin();
            }


        } else {
            gotoSetupActivity();
        }

    }

    private void gotoSetupActivity() {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Urls.Get_Access_Token, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseJson(response);
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("response", "Error: " + error.toString());
                showToast(getString(R.string.error));
                finish();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", Constants.Content_Type);
                headers.put("Authorization", Constants.Authorization);
                return headers;
            }
        };
        FriendShipApplication.getInstance().addToRequestQueue(strReq);
    }

    private void parseJson(String json) {

        try {

            JSONObject jsonObject = new JSONObject(json);
            Preference.getInstance().put(SplashActivity.this, PrefConst.Key_access_token, jsonObject.getString("access_token"));
            Preference.getInstance().put(SplashActivity.this, PrefConst.Key_token_type, jsonObject.getString("token_type"));
            Preference.getInstance().put(SplashActivity.this, PrefConst.Key_expires_in, jsonObject.getString("expires_in"));
            Preference.getInstance().put(SplashActivity.this, PrefConst.Key_scope, jsonObject.getString("scope"));

            if (Preference.getInstance().getValue(SplashActivity.this, PrefConst.Key_Is_Registering_page, 0) == 1) {
                startActivity(new Intent(SplashActivity.this, FirstNameActivity.class));
            } else if (Preference.getInstance().getValue(SplashActivity.this, PrefConst.Key_Is_Registering_page, 0) == 2) {
                startActivity(new Intent(SplashActivity.this, MobileNumberActivity.class));
            } else if (Preference.getInstance().getValue(SplashActivity.this, PrefConst.Key_Is_Registering_page, 0) == 3) {
                startActivity(new Intent(SplashActivity.this, MobileNumberActivity.class));
            } else if (Preference.getInstance().getValue(SplashActivity.this, PrefConst.Key_Is_Registering_page, 0) == 4) {
                startActivity(new Intent(SplashActivity.this, UploadImageActivity.class));
            } else if (Preference.getInstance().getValue(SplashActivity.this, PrefConst.Key_Is_Registering_page, 0) == 5) {
                startActivity(new Intent(SplashActivity.this, ContactActivity.class));
            } else if (Preference.getInstance().getValue(SplashActivity.this, PrefConst.Key_Is_Registering_page, 0) == 6) {
                startActivity(new Intent(SplashActivity.this, ContactListActivity.class));
            } else {
                startActivity(new Intent(SplashActivity.this, StartSetupActivity.class));

            }
            finish();

            overridePendingTransition(0, 0);


        } catch (JSONException e) {

            e.printStackTrace();
            showToast(getString(R.string.error));
            finish();

        }
    }

    private void dologin() {

        final String username = Preference.getInstance().getValue(SplashActivity.this, PrefConst.Key_login, PrefConst.Key_login);
        final String password = Preference.getInstance().getValue(SplashActivity.this, PrefConst.Key_password, PrefConst.Key_password);
        final String grant_type = "password";
        final String scope = "read write";
        final String client_secret = "7vc6c8cvnvc7vc";
        final String client_id = "bbApp";

        /*String params = "username=" + username + "&password=" + password + "&grant_type="
                + grant_type + "&scope=" + scope + "&client_secret=" + client_secret + "&client_id=" + client_id;
        showProgress(false);*/
        final String loginMobile = Preference.getInstance().getValue(SplashActivity.this, PrefConst.Key_login, PrefConst.Key_login);


        StringRequest jsonObjRequest = new StringRequest(Request.Method.POST,
                Urls.Login,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", "" + response);
                        closeProgress();
                        try {
                            JSONObject obj = new JSONObject(response);
                            parseJsonLogin(obj);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("volley", "Error: " + error.getMessage());
                error.printStackTrace();
                gotoSetupActivity();


            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", loginMobile);
                params.put("password", password);
                params.put("grant_type", grant_type);
                params.put("scope", scope);
                params.put("client_secret", client_secret);
                params.put("client_id", client_id);
                Log.e("Params", "" + params.toString());
                return params;
            }

        };
        FriendShipApplication.getInstance().addToRequestQueue(jsonObjRequest);

    }

    private void parseJsonLogin(JSONObject jsonObject) {
        try {


            Preference.getInstance().put(SplashActivity.this, PrefConst.Key_access_token, jsonObject.getString("access_token"));
            Preference.getInstance().put(SplashActivity.this, PrefConst.Key_token_type, jsonObject.getString("token_type"));
            Preference.getInstance().put(SplashActivity.this, PrefConst.Key_refreshtoken, jsonObject.getString("refresh_token"));
            Preference.getInstance().put(SplashActivity.this, PrefConst.Key_expires_in, jsonObject.getString("expires_in"));
            Preference.getInstance().put(SplashActivity.this, PrefConst.Key_scope, jsonObject.getString("scope"));

            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            finish();
            overridePendingTransition(0, 0);
            /*startActivity(new Intent(SplashActivity.this, UploadImageActivity.class));
            finish();

          */

        } catch (JSONException e) {
            e.printStackTrace();
            showToast(getString(R.string.error));
        }
    }

}
