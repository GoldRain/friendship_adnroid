package com.friendship.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.friendship.FriendShipApplication;
import com.friendship.R;
import com.friendship.base.CommonActivity;
import com.friendship.commons.Constants;
import com.friendship.commons.Urls;
import com.friendship.preference.PrefConst;
import com.friendship.preference.Preference;
import com.friendship.utils.BitmapUtils;
import com.friendship.utils.PhotoSelectDialog;
import com.friendship.utils.TouchImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class UploadImageActivity extends CommonActivity implements View.OnClickListener {

    RelativeLayout ryt_next;
    TouchImageView imv_takephoto;

    private Bitmap bitmap = null;


    private Uri _imageCaptureUri;
    private String _photoPath = "";
    private PhotoSelectDialog _photoSelectDialog;

    TextView txv_loadimage;

    //String[] PERMISSIONS = {android.Manifest.permission.CAMERA};

    String[] PERMISSIONS = {android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_SMS};

    String _gallery = "";
    String _camera = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_image);
        Preference.getInstance().put(UploadImageActivity.this, PrefConst.Key_Is_Registering_page, 4);

        loadLayout();
    }

    private void loadLayout() {
        String base64=Preference.getInstance().getValue(UploadImageActivity.this, PrefConst.Key_ImageBase64, PrefConst.Key_ImageBase64);

        imv_takephoto = (TouchImageView) findViewById(R.id.imv_takephoto);
        imv_takephoto.setOnClickListener(this);

        ryt_next = (RelativeLayout) findViewById(R.id.ryt_next);
        ryt_next.setOnClickListener(this);

        txv_loadimage = (TextView) findViewById(R.id.txv_loadimage);

        if(!base64.matches("base64"))
        {
            byte[] imageAsBytes = Base64.decode(base64.getBytes(), Base64.DEFAULT);
            imv_takephoto.setImageBitmap(
                    BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length)
            );
            txv_loadimage.setText("");
        }


    }

    /*==================== Permission========================================*/
    public void checkAllPermission() {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (_camera.length() > 0) {

            if (hasPermissions(this, PERMISSIONS)) {

                if (_camera.length() > 0) {

                    _camera = "";
                    doTakePhotoAction();

                } else if (_gallery.length() > 0) {
                    _gallery = "";
                    doTakeGallery();
                }


            } else {

                ActivityCompat.requestPermissions(this, PERMISSIONS, Constants.PERMISSION);
            }

        } else if (_gallery.length() > 0) {


            if (hasPermissions(this, PERMISSIONS)) {

                if (_camera.length() > 0) {

                    _camera = "";
                    doTakePhotoAction();

                } else if (_gallery.length() > 0) {
                    _gallery = "";
                    doTakeGallery();
                }


            } else {
                ActivityCompat.requestPermissions(this, PERMISSIONS, Constants.PERMISSION2);
            }

        }


    }

    /*//////////////////////////////////////////////////////////////////////////////*/

    public boolean hasPermissions(Context context, String... permissions) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {

            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {

                    return false;

                }
            }
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {

            case Constants.PERMISSION: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(this, "Camera permission granted",
                            Toast.LENGTH_SHORT).show();


                    if (_camera.length() > 0) {

                        _camera = "";
                        doTakePhotoAction();

                    } else if (_gallery.length() > 0) {
                        _gallery = "";
                        //doTakeGallery();
                    }
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    Toast.makeText(this, "Camera permission denied",
                            Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }

                return;

            }
            case Constants.PERMISSION2: {
                doTakeGallery();
                break;

            }


            // other 'case' lines to check for other
            // permissions this app might request

        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void selectPhoto() {

        final String[] items = {"Take photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                    _camera = "camera";

                    //getPermissionToReadUserContacts();
                    checkAllPermission();
                    //doTakePhotoAction();

                } else if (item == 1) {
                    //doTakeGallery();
                    _gallery = "gallery";
                    checkAllPermission();

                } else {
                    return;
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }


    public void doTakePhotoAction() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

    }

    public void doTakeGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);

    }

    protected void onActivityResult(int requestCOde, int resultCode, Intent data) {

        switch (requestCOde) {

            case Constants.CROP_FROM_CAMERA:

                if (resultCode == RESULT_OK) {
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        bitmap = BitmapFactory.decodeStream(in, null, bitOpt);

                        in.close();
                        imv_takephoto.setImageResource(0);
                        //set The bitmap data to image View
                        imv_takephoto.setImageBitmap(bitmap);
                        _photoPath = saveFile.getAbsolutePath();
                        txv_loadimage.setText("");


                 /*       if (_photoPath.length() != 0) {

                            txv_loadimage.setText("");
                        }*/

                        //uploadImage();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                break;
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK) {
                    _imageCaptureUri = data.getData();
                    try {

                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), _imageCaptureUri);
                        imv_takephoto.setImageResource(0);
                        imv_takephoto.setImageBitmap(bitmap);
                        txv_loadimage.setText("");


               /*         if (_photoPath.length() != 0) {

                            txv_loadimage.setText("");
                        }*/
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                break;
            case Constants.PICK_FROM_CAMERA:
                try {
                    _photoPath = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    //intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }

    }

    private void gotoValidate() {

        //   startActivity(new Intent(this, ValidateActivity.class));
        //   finish();

        //   overridePendingTransition(0, 0);
    }

    private void gotoContact() throws JSONException {

        if (bitmap != null) {
            ryt_next.setEnabled(false);
            gotoUpload();

        } else {
            startActivity(new Intent(this, ContactActivity.class));
            finish();

            overridePendingTransition(0, 0);
        }
    }

    private void gotoUpload() throws JSONException {

        showProgress(false);
        final String login = Preference.getInstance().getValue(UploadImageActivity.this, PrefConst.Key_login, PrefConst.Key_login);
        String base64=encodeTobase64(bitmap);
        Preference.getInstance().put(UploadImageActivity.this, PrefConst.Key_ImageBase64,base64+ "");

        JSONObject objMainList = new JSONObject();
        JSONArray scope = new JSONArray();


        objMainList.put("login", login + "");
        objMainList.put("base64Image",base64+ "");
        objMainList.put("countryCode", getResources().getConfiguration().locale.getCountry());
        objMainList.put("firstName", Preference.getInstance().getValue(UploadImageActivity.this, PrefConst.Key_firstName, PrefConst.Key_firstName));
        objMainList.put("lastName", Preference.getInstance().getValue(UploadImageActivity.this, PrefConst.Key_lastName, PrefConst.Key_lastName));
        objMainList.put("langKey", Locale.getDefault().getDisplayLanguage().toLowerCase().substring(0, 2));
        objMainList.put("activated", true);

        scope.put("" + Preference.getInstance().getValue(UploadImageActivity.this, PrefConst.Key_scope, PrefConst.Key_scope));
        objMainList.put("authorities", scope);

        JsonRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Urls.Send_Image, objMainList,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Responce ", response.toString());
                        startActivity(new Intent(UploadImageActivity.this, ContactActivity.class));
                        finish();

                        overridePendingTransition(0, 0);
                        closeProgress();
                        ryt_next.setEnabled(true);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ryt_next.setEnabled(true);
                closeProgress();
                if (error.getMessage() != null) {

                    if (error.getMessage().contains("character 0 of")) {

                        startActivity(new Intent(UploadImageActivity.this, ContactActivity.class));
                        finish();

                        overridePendingTransition(0, 0);
                        showToast(getString(R.string.photo_upload));

                    } else {
                        showToast(getString(R.string.upload_error));

                    }
                }
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", Constants.Content_Type);
                headers.put("Content-Type", Constants.Content_Type);
                headers.put("Authorization", Preference.getInstance().getValue(UploadImageActivity.this, PrefConst.Key_token_type, PrefConst.Key_token_type)
                        + " " + Preference.getInstance().getValue(UploadImageActivity.this, PrefConst.Key_access_token, PrefConst.Key_access_token));
                return headers;
            }
        };
        FriendShipApplication.getInstance().addToRequestQueue(jsonObjReq);

    }

    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.imv_takephoto:

                selectPhoto();
                break;

            case R.id.ryt_next:
                try {
                    gotoContact();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {

        gotoValidate();
    }
}
