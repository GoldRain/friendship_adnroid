package com.friendship.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.friendship.FriendShipApplication;
import com.friendship.R;
import com.friendship.base.CommonActivity;
import com.friendship.commons.Constants;
import com.friendship.commons.Urls;
import com.friendship.preference.PrefConst;
import com.friendship.preference.Preference;
import com.friendship.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class ValidateActivity extends CommonActivity implements View.OnClickListener {

    EditText edt_digit1, edt_digit2, edt_digit3, edt_digit4;
    RelativeLayout ryt_next;

    int current = 0;
    int _digit1 = 0, _digit2 = 0, _digit3 = 0, _digit4 = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate);
        Preference.getInstance().put(ValidateActivity.this, PrefConst.Key_Is_Registering_page, 3);
        current = 5;
        loadLayout();
    }

    private void loadLayout() {

        edt_digit1 = (EditText) findViewById(R.id.edt_digit1);
        edt_digit2 = (EditText) findViewById(R.id.edt_digit2);
        edt_digit3 = (EditText) findViewById(R.id.edt_digit3);
        edt_digit4 = (EditText) findViewById(R.id.edt_digit4);
        edt_digit1.setText("" + 1);
        edt_digit2.setText("" + 2);
        edt_digit3.setText("" + 3);
        edt_digit4.setText("" + 4);

        edt_digit1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (edt_digit1.getText().length() != 0) {

                    edt_digit2.requestFocus();
                    _digit1 = 1;
                }
            }
        });

        edt_digit1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {

                    if (_digit1 == 1) {
                        edt_digit1.setText("");
                        _digit1 = 0;
                    }
                }
            }
        });


        edt_digit2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (edt_digit2.getText().length() != 0) {
                    edt_digit3.requestFocus();
                    _digit2 = 1;
                }
            }
        });

        edt_digit2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (_digit2 == 1) {
                        edt_digit2.setText("");
                        _digit2 = 0;
                    }
                }
            }
        });


        edt_digit3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (edt_digit3.getText().length() != 0) {
                    edt_digit4.requestFocus();

                    _digit3 = 1;
                }
            }
        });

        edt_digit3.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {

                    if (_digit3 == 1) {
                        edt_digit3.setText("");
                        _digit3 = 0;
                    }
                }
            }
        });

        edt_digit4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (edt_digit4.getText().length() != 0) {
                    _digit4 = 1;
                }
            }
        });

        edt_digit4.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (_digit4 == 1) {
                        edt_digit4.setText("");
                        _digit4 = 0;
                    }
                }
            }
        });


        ryt_next = (RelativeLayout) findViewById(R.id.ryt_next);
        ryt_next.setOnClickListener(this);

        RelativeLayout lyt_container = (RelativeLayout) findViewById(R.id.activity_validate);
        lyt_container.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_digit1.getWindowToken(), 0);
                return false;
            }
        });
    }

    public boolean checkValid() {

        if (edt_digit1.getText().toString().length() == 0 || edt_digit2.getText().toString().length() == 0
                || edt_digit3.getText().toString().length() == 0 || edt_digit4.getText().toString().length() == 0) {

            showAlertDialog(getString(R.string.digits_code));

            return false;
        }

        return true;
    }

    private void gotoMobileNumber() {

/*        startActivity(new Intent(this, MobileNumberActivity.class));
        finish();

        overridePendingTransition(0, 0);*/
    }

    private void gotoUpload() {

        final String MsgCode = edt_digit1.getText().toString() + edt_digit2.getText().toString()
                + edt_digit3.getText().toString() + edt_digit4.getText().toString();
        final String login = Preference.getInstance().getValue(ValidateActivity.this, PrefConst.Key_login, PrefConst.Key_login);
        String encoded = Uri.encode(login);
        String params = "key=" + MsgCode + "&login=" + encoded;
        showProgress(false);

        JsonRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                Urls.Send_Msg_Code + params,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        closeProgress();
                        showToast(getString(R.string.Msgcode_success));
                        parseJson(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                closeProgress();
                String body = "";
                //get status code here
                String statusCode = "";
                //get response body and parse with appropriate encoding

                try {
                    if (error != null) {
                        if (error.networkResponse.data != null) {
                            statusCode = String.valueOf(error.networkResponse.statusCode);
                            body = new String(error.networkResponse.data, "UTF-8");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (statusCode.contains("401")) {
                    getAccessToken();
                } else {
                    showToast(getString(R.string.access_denie));
                }
            }
        }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", Constants.Content_Type);
                headers.put("Authorization", Constants.Authorization);
                headers.put("Authorization", Preference.getInstance().getValue(ValidateActivity.this, PrefConst.Key_token_type, PrefConst.Key_token_type)
                        + " " + Preference.getInstance().getValue(ValidateActivity.this, PrefConst.Key_access_token, PrefConst.Key_access_token));
                return headers;
            }
        };
        FriendShipApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    private void parseJson(JSONObject jsonObject) {

        try {

            Preference.getInstance().put(ValidateActivity.this, PrefConst.Key_userId, jsonObject.getInt("userId"));
            Preference.getInstance().put(ValidateActivity.this, PrefConst.Key_login, jsonObject.getString("login"));
            Preference.getInstance().put(ValidateActivity.this, PrefConst.Key_password, jsonObject.getString("password"));

            dologin();

            /*startActivity(new Intent(ValidateActivity.this, UploadImageActivity.class));
            finish();

            overridePendingTransition(0,0);*/

        } catch (JSONException e) {
            e.printStackTrace();
            showToast(getString(R.string.error));
        }
    }


    private void parseJsonLogin(JSONObject jsonObject) {
        try {


            Preference.getInstance().put(ValidateActivity.this, PrefConst.Key_access_token, jsonObject.getString("access_token"));
            Preference.getInstance().put(ValidateActivity.this, PrefConst.Key_token_type, jsonObject.getString("token_type"));
            Preference.getInstance().put(ValidateActivity.this, PrefConst.Key_refreshtoken, jsonObject.getString("refresh_token"));
            Preference.getInstance().put(ValidateActivity.this, PrefConst.Key_expires_in, jsonObject.getString("expires_in"));
            Preference.getInstance().put(ValidateActivity.this, PrefConst.Key_scope, jsonObject.getString("scope"));

            startActivity(new Intent(ValidateActivity.this, UploadImageActivity.class));
            finish();
            overridePendingTransition(0, 0);
            /*startActivity(new Intent(ValidateActivity.this, UploadImageActivity.class));
            finish();

          */

        } catch (JSONException e) {
            e.printStackTrace();
            showToast(getString(R.string.error));
        }
    }

    private void dologin() {

        final String username = Preference.getInstance().getValue(ValidateActivity.this, PrefConst.Key_login, PrefConst.Key_login);
        final String password = Preference.getInstance().getValue(ValidateActivity.this, PrefConst.Key_password, PrefConst.Key_password);
        final String grant_type = "password";
        final String scope = "read write";
        final String client_secret = "7vc6c8cvnvc7vc";
        final String client_id = "bbApp";

        /*String params = "username=" + username + "&password=" + password + "&grant_type="
                + grant_type + "&scope=" + scope + "&client_secret=" + client_secret + "&client_id=" + client_id;
        showProgress(false);*/
        final String loginMobile = Preference.getInstance().getValue(ValidateActivity.this, PrefConst.Key_login, PrefConst.Key_login);


        StringRequest jsonObjRequest = new StringRequest(Request.Method.POST,
                Urls.Login,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", "" + response);
                        closeProgress();
                        try {
                            JSONObject obj = new JSONObject(response);
                            parseJsonLogin(obj);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("volley", "Error: " + error.getMessage());
                error.printStackTrace();


            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", loginMobile);
                params.put("password", password);
                params.put("grant_type", grant_type);
                params.put("scope", scope);
                params.put("client_secret", client_secret);
                params.put("client_id", client_id);
                Log.e("Params", "" + params.toString());
                return params;
            }

        };
        FriendShipApplication.getInstance().addToRequestQueue(jsonObjRequest);

    }

    private void getAccessToken() {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Urls.Get_Access_Token, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseJson(response);
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("response", "Error: " + error.toString());
                showToast(getString(R.string.error));
                finish();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", Constants.Content_Type);
                headers.put("Authorization", Constants.Authorization);
                return headers;
            }
        };
        FriendShipApplication.getInstance().addToRequestQueue(strReq);
    }

    private void parseJson(String json) {

        try {

            JSONObject jsonObject = new JSONObject(json);

            Preference.getInstance().put(ValidateActivity.this, PrefConst.Key_access_token, jsonObject.getString("access_token"));
            Preference.getInstance().put(ValidateActivity.this, PrefConst.Key_token_type, jsonObject.getString("token_type"));
            Preference.getInstance().put(ValidateActivity.this, PrefConst.Key_expires_in, jsonObject.getString("expires_in"));
            Preference.getInstance().put(ValidateActivity.this, PrefConst.Key_scope, jsonObject.getString("scope"));

            gotoUpload();
        } catch (JSONException e) {

            e.printStackTrace();
            showToast(getString(R.string.error));
            finish();

        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.ryt_next:
                if (Utils.isOnline(ValidateActivity.this)) {
                    if (checkValid())
                        gotoUpload();
                }else{
                    showToast(getString(R.string.error_internet));
                }

                break;
        }

    }

    @Override
    public void onBackPressed() {

        gotoMobileNumber();
    }
}
