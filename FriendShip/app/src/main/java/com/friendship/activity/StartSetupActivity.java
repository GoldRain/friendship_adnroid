package com.friendship.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.friendship.R;
import com.friendship.base.CommonActivity;

public class StartSetupActivity extends CommonActivity implements View.OnClickListener{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_setup);
    }

    public void gotoMain(View view){

        startActivity(new Intent(this, MainActivity.class));
        finish();

        overridePendingTransition(0,0);
    }

    public void gotoFirstName(View view){

        startActivity(new Intent(this, FirstNameActivity.class));
        finish();

        overridePendingTransition(0,0);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            onExit();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
