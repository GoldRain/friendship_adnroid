package com.friendship.activity;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.friendship.FriendShipApplication;
import com.friendship.R;
import com.friendship.adapter.AddContactListAdapter;
import com.friendship.base.CommonActivity;
import com.friendship.commons.Constants;
import com.friendship.commons.Urls;
import com.friendship.model.FriendModel;
import com.friendship.model.FriendModelAPI;
import com.friendship.preference.PrefConst;
import com.friendship.preference.Preference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.friendship.commons.Urls.Receive_FriendList;

public class ContactListActivity extends CommonActivity implements View.OnClickListener {

    ArrayList<FriendModel> _friends = new ArrayList<>();

    ArrayList<FriendModelAPI> _friendsapi = new ArrayList<>();

    ListView lst_contact;
    AddContactListAdapter _adapter;
    RelativeLayout ryt_next;

    ImageView imv_search, imv_search_back;
    EditText edt_search;
    TextView txv_title;
    TextView txv_number;

    ImageView imv_line;
    int index = 0;
   /* String[] PERMISSIONS = {Manifest.permission.READ_SMS, Manifest.permission.WRITE_CONTACTS, Manifest.permission.READ_CONTACTS, Manifest.permission.CALL_PHONE,
                                Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.INTERNET};*/

    public static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 101;

    private static final String[] PERMISSIONS = {
            Manifest.permission.SEND_SMS, Manifest.permission.READ_SMS, Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_CONTACTS,
            Manifest.permission.READ_CONTACTS};

    public static final int REQUEST_CODE = 0;
    private RelativeLayout mainLayout;

    int pos = -1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);
        Preference.getInstance().put(ContactListActivity.this, PrefConst.Key_Is_Registering_page, 6);
        loadLayout();
    }

    private void loadLayout() {

        mainLayout = (RelativeLayout) findViewById(R.id.activity_contact_list);


        lst_contact = (ListView) findViewById(R.id.lst_contact);
        _adapter = new AddContactListAdapter(this);
        lst_contact.setAdapter(_adapter);

        lst_contact.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                _adapter.setSelectedIndex(position);


            }
        });

        ryt_next = (RelativeLayout) findViewById(R.id.ryt_next);
        ryt_next.setOnClickListener(this);

        imv_search = (ImageView) findViewById(R.id.imv_search);
        imv_search.setOnClickListener(this);

        imv_search_back = (ImageView) findViewById(R.id.imv_search_back);
        imv_search_back.setOnClickListener(this);

        edt_search = (EditText) findViewById(R.id.edt_search);

        imv_line = (ImageView) findViewById(R.id.imv_line);

        txv_title = (TextView) findViewById(R.id.txv_title);
        txv_number = (TextView) findViewById(R.id.txv_number);

        imv_search_back.setVisibility(View.GONE);
        txv_title.setText(getString(R.string.invite_friend));
        edt_search.setText("");
        edt_search.setVisibility(View.GONE);
        imv_line.setVisibility(View.GONE);


        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                _adapter.filter(edt_search.getText().toString());
            }
        });
        readContact();
        txv_number.setText(String.valueOf("0"));


    }

    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }
    //=========================== PermissionCheck=============================

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    readContacts();
                    // permission was granted, yay!

                } else {


                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    public void checkAllPermission() {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (hasPermissions(this, PERMISSIONS)) {
            readContacts();
        } else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {

            for (String permission : permissions) {

                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void readContacts() {
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                String image_uri = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));

                String phone = "";
                String email="";
                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    System.out.println("name : " + name + ", ID : " + id);

                    // get the phone number
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        phone = pCur.getString(
                                pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        System.out.println("phone" + phone);
                    }
                    pCur.close();


                    // get email and type

                    Cursor emailCur = cr.query(
                            ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (emailCur.moveToNext()) {
                        // This would allow you get several email addresses
                        // if the email addresses were stored in an array
                        email = emailCur.getString(
                                emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                        String emailType = emailCur.getString(
                                emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));

                        System.out.println("Email " + email + " Email Type : " + emailType);
                    }
                    emailCur.close();


                }

                FriendModel friendModel = new FriendModel();
                if (isValidMobile(phone)) {
                    friendModel.set_name(name);
                    friendModel.set_id(Integer.parseInt(id));
                    friendModel.set_photo_url(image_uri);
                    friendModel.set_phonnumber(phone);
                    friendModel.setEmail(email);
                    friendModel.setFromPhone("true");
                    friendModel.setPlusminus("plus");
                    _friends.add(friendModel);

                }

            }
            for (FriendModelAPI person2 : _friendsapi) {
                pos = pos + 1;
                for (FriendModel person1 : _friends) {

                    if (PhoneNumberUtils.compare(person2.get_phonnumber(), person1.get_phonnumber())) {
                        person1.set_id(person2.get_id());
                        person1.set_name(person2.get_name());
                        person1.set_photo_url(person2.get_photo_url());
                        person1.set_phonnumber(person2.get_phonnumber());
                        person1.setFromPhone(person2.getFromPhone());
                        person1.setPlusminus(person2.getPlusminus());
                        _friends.set(pos, person1);
                    }
                }

            }

            _adapter.setFriends(_friends);
            txv_number.setText(String.valueOf("0"));
            _adapter.notifyDataSetChanged();
        }
    }


    private void gotoMain() {

        startActivity(new Intent(this, MainActivity.class));
        finish();

        overridePendingTransition(0, 0);
    }

    private void gotoContact() {

        startActivity(new Intent(this, ContactActivity.class));
        finish();

        overridePendingTransition(0, 0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imv_search_back:

                txv_title.setVisibility(View.VISIBLE);
                txv_title.setText(getString(R.string.invite_friend));

                edt_search.setText("");
                edt_search.setVisibility(View.GONE);
                imv_line.setVisibility(View.GONE);
                imv_search_back.setVisibility(View.GONE);
                imv_search.setVisibility(View.VISIBLE);
                hideKeyboard();
                break;

            case R.id.imv_search:

                imv_search_back.setVisibility(View.VISIBLE);
                imv_search.setVisibility(View.GONE);
                edt_search.setVisibility(View.VISIBLE);
                edt_search.requestFocus();
                imv_line.setVisibility(View.VISIBLE);

                txv_title.setText("");
                txv_title.setVisibility(View.GONE);
                showKeyboard();
                break;

            case R.id.ryt_next:
                if (_friends.size() > 1) {
                    try {
                        doSendContactRegister();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    gotoMain();
                }
                break;
        }
    }

    private void readContact() {


        showProgress(false);

        JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET,
                Receive_FriendList,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e("Validate Responce ", "" + response.toString());
                        closeProgress();
                        try {
                            parseJsonContact(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }


                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Validate Error", "" + error.getMessage());
                closeProgress();
                checkAllPermission();
            }
        }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", Constants.Content_Type);
                // headers.put("Authorization", Constants.Authorization);
                headers.put("Authorization", Preference.getInstance().getValue(ContactListActivity.this, PrefConst.Key_token_type, PrefConst.Key_token_type)
                        + " " + Preference.getInstance().getValue(ContactListActivity.this, PrefConst.Key_access_token, PrefConst.Key_access_token));
                return headers;
            }
        };
        FriendShipApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    private void parseJsonContact(JSONArray response) throws JSONException {
        for (int i = 0; i < response.length(); i++) {
            JSONObject contactJsonObject = response.getJSONObject(i);
            FriendModelAPI friendModel = new FriendModelAPI();
            friendModel.set_id(contactJsonObject.getInt("userId"));
            friendModel.set_firstName(contactJsonObject.getString("firstName"));
            friendModel.set_lastName(contactJsonObject.getString("lastName"));
            friendModel.set_phonnumber(contactJsonObject.getString("mobileNumber"));
            friendModel.set_name(contactJsonObject.getString("firstName") + " " + contactJsonObject.getString("lastName"));
            friendModel.setFromPhone("false");
            friendModel.setPlusminus("plus");

            _friendsapi.add(friendModel);

        }
        checkAllPermission();
    }

    public void doSendContactRegister() throws JSONException {


        showProgress(false);
        JSONObject objMainList = new JSONObject();


        JSONArray contact = new JSONArray();

        for (int i = 0; i < _friends.size(); i++) {

            JSONObject contactObject = new JSONObject();
            if (_friends.get(i).getPlusminus().matches("minus")) {

                Log.e("email", "" + _friends.get(i).getEmail());

                if (_friends.get(i).getEmail() != null) {

                    contactObject.put("eMail", _friends.get(i).getEmail() + "");

                } else {

                    contactObject.put("eMail", "info@friend-ship.com");
                }

                contactObject.put("firstName", _friends.get(i).get_firstName() + "");
                contactObject.put("lastName", _friends.get(i).get_lastName() + "");
                contactObject.put("mobileNumber", _friends.get(i).get_phonnumber() + "");
                contactObject.put("userId", _friends.get(i).get_id() + "");
                contact.put(contactObject);
            }
        }

        objMainList.put("login", Preference.getInstance().getValue(ContactListActivity.this, PrefConst.Key_login, PrefConst.Key_login));
        objMainList.put("inviteFriends", contact);


        Log.e("Params", "" + objMainList.toString());


        JsonRequest jsonObjReq = new JsonObjectRequest(Request.Method.PUT,
                Urls.Send_Contact, objMainList,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Responce ", response.toString());
                        closeProgress();


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", "Error: " + error.getMessage());

                closeProgress();
                if (error.getMessage() != null) {
                    if (error.getMessage().contains("character 0 of")) {

                        startActivity(new Intent(ContactListActivity.this, MainActivity.class));
                        finish();
                        overridePendingTransition(0, 0);
                    }
                } else {
                    showToast(getString(R.string.invite_friend_failed));
                }
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", Constants.Content_Type);
                // headers.put("Authorization", Constants.Authorization);
                headers.put("Authorization", Preference.getInstance().getValue(ContactListActivity.this, PrefConst.Key_token_type, PrefConst.Key_token_type)
                        + " " + Preference.getInstance().getValue(ContactListActivity.this, PrefConst.Key_access_token, PrefConst.Key_access_token));
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 10, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        FriendShipApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);
    }

    private void showKeyboard() {
        InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        im.showSoftInput(edt_search, InputMethodManager.SHOW_FORCED);
    }

    @Override
    public void onBackPressed() {
        gotoContact();
    }
}
