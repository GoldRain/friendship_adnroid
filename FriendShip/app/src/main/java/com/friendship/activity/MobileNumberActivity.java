package com.friendship.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.util.Util;
import com.friendship.FriendShipApplication;
import com.friendship.R;
import com.friendship.base.CommonActivity;
import com.friendship.commons.Constants;
import com.friendship.commons.Urls;
import com.friendship.preference.PrefConst;
import com.friendship.preference.Preference;
import com.friendship.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MobileNumberActivity extends CommonActivity implements View.OnClickListener {

    EditText edt_mobileNumber;
    RelativeLayout ryt_next;

    String[] PERMISSIONS = {Manifest.permission.READ_PHONE_STATE,Manifest.permission.GET_ACCOUNTS};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_number);
        Preference.getInstance().put(MobileNumberActivity.this, PrefConst.Key_Is_Registering_page, 2);
        loadLayout();

    }


    private void loadLayout() {

        edt_mobileNumber = (EditText) findViewById(R.id.edt_phone);
        edt_mobileNumber.addTextChangedListener(new MyTextWatcher(edt_mobileNumber));

        ryt_next = (RelativeLayout) findViewById(R.id.ryt_next);
        ryt_next.setOnClickListener(this);

        if (hasPermissions(this, PERMISSIONS)) {

            GetMobileNumberFromTelephony();

        } else {

            ActivityCompat.requestPermissions(this, PERMISSIONS, Constants.PERMISSION);
        }

        validateTextField();

        RelativeLayout lyt_container = (RelativeLayout) findViewById(R.id.activity_mobile_number);
        lyt_container.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_mobileNumber.getWindowToken(), 0);
                return false;
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {

            case Constants.PERMISSION: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    GetMobileNumberFromTelephony();

                } else {

                    showToast(getString(R.string.permission_error));

                }

                return;

            }
            case Constants.PERMISSION2: {

                break;

            }

        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void gotoFirstName() {

        startActivity(new Intent(this, FirstNameActivity.class));
        finish();
        overridePendingTransition(0, 0);
    }

    private void GetMobileNumberFromTelephony() {

        TelephonyManager telemamanger = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String getSimSerialNumber = telemamanger.getSimSerialNumber();
        String getSimNumber = telemamanger.getLine1Number();
        Log.e("Mobile Number", "" + getSimNumber);
        if (getSimNumber != null && getSimNumber != "") {
            edt_mobileNumber.setText(getSimNumber);
        }

    }

    private static boolean isValidMobile(String mobile) {
        return !TextUtils.isEmpty(mobile) && Patterns.PHONE.matcher(mobile).matches();
    }

    private void gotoValidate() {

        if (Utils.isOnline(MobileNumberActivity.this)) {

            if (isValidMobile(edt_mobileNumber.getText().toString())) {
                doRegister();
            } else {
                showToast(getString(R.string.validate_number));
            }

        } else {
            showToast(getString(R.string.error_internet));
        }
    }

    public boolean hasPermissions(Context context, String... permissions) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {

            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {

                    return false;

                }
            }
        }
        return true;
    }

    public void doRegister() {

        Preference.getInstance().put(MobileNumberActivity.this, PrefConst.Key_login, edt_mobileNumber.getText().toString());

        showProgress(false);

        Map<String, String> params = new HashMap<String, String>();
        params.put("firstName", Preference.getInstance().getValue(MobileNumberActivity.this, PrefConst.Key_firstName, PrefConst.Key_firstName));
        params.put("lastName", Preference.getInstance().getValue(MobileNumberActivity.this, PrefConst.Key_lastName, PrefConst.Key_lastName));
        // params.put("langKey", Locale.getDefault().getDisplayLanguage().toLowerCase()+"");
        params.put("langKey", Locale.getDefault().getDisplayLanguage().toLowerCase().substring(0, 2));
        params.put("login", Preference.getInstance().getValue(MobileNumberActivity.this, PrefConst.Key_login, PrefConst.Key_login));
        params.put("countryCode", getResources().getConfiguration().locale.getCountry());
        Log.e("Params", "" + params.toString());


        JsonRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Urls.Register, new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Responce ", response.toString());
                        startActivity(new Intent(MobileNumberActivity.this, ValidateActivity.class));
                        finish();
                        overridePendingTransition(0, 0);
                        closeProgress();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                closeProgress();
                String body = "";
                //get status code here
                String statusCode = "";
                //get response body and parse with appropriate encoding

                try {
                    if (error != null) {
                        if (error.networkResponse.data != null) {
                            statusCode = String.valueOf(error.networkResponse.statusCode);
                            body = new String(error.networkResponse.data, "UTF-8");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (error.getMessage() != null) {

                    if (statusCode.contains("201")) {

                        startActivity(new Intent(MobileNumberActivity.this, ValidateActivity.class));
                        finish();
                        overridePendingTransition(0, 0);
                    } else if (statusCode.contains("401")) {
                        getAccessToken();

                    }

                } else if (statusCode.contains("401")) {
                    getAccessToken();

                } else {
                    showToast(getString(R.string.alrady_register));
                }
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", Constants.Content_Type);
                headers.put("Authorization", Constants.Authorization);
                headers.put("Authorization", Preference.getInstance().getValue(MobileNumberActivity.this, PrefConst.Key_token_type, PrefConst.Key_token_type)
                        + " " + Preference.getInstance().getValue(MobileNumberActivity.this, PrefConst.Key_access_token, PrefConst.Key_access_token));
                return headers;
            }
        };
        FriendShipApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    private void getAccessToken() {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Urls.Get_Access_Token, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseJson(response);
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("response", "Error: " + error.toString());
                showToast(getString(R.string.error));
                finish();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", Constants.Content_Type);
                headers.put("Authorization", Constants.Authorization);
                return headers;
            }
        };
        FriendShipApplication.getInstance().addToRequestQueue(strReq);
    }

    private void parseJson(String json) {

        try {

            JSONObject jsonObject = new JSONObject(json);

            Preference.getInstance().put(MobileNumberActivity.this, PrefConst.Key_access_token, jsonObject.getString("access_token"));
            Preference.getInstance().put(MobileNumberActivity.this, PrefConst.Key_token_type, jsonObject.getString("token_type"));
            Preference.getInstance().put(MobileNumberActivity.this, PrefConst.Key_expires_in, jsonObject.getString("expires_in"));
            Preference.getInstance().put(MobileNumberActivity.this, PrefConst.Key_scope, jsonObject.getString("scope"));
            doRegister();
        } catch (JSONException e) {

            e.printStackTrace();
            showToast(getString(R.string.error));
            finish();

        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.ryt_next:
                gotoValidate();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        gotoFirstName();
    }

    private class MyTextWatcher implements TextWatcher {
        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.edt_phone:
                    validateTextField();
                    break;
            }
        }
    }

    private void validateTextField() {
        if (edt_mobileNumber.getText().toString().trim().length() <= 0) {
          //  ryt_next.setEnabled(false);
        } else {
           // ryt_next.setEnabled(true);
        }
    }

}
