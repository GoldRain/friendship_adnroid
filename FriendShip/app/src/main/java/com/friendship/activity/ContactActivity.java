package com.friendship.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.friendship.R;
import com.friendship.base.CommonActivity;
import com.friendship.preference.PrefConst;
import com.friendship.preference.Preference;

public class ContactActivity extends CommonActivity  {

    RelativeLayout rlt_next;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        Preference.getInstance().put(ContactActivity.this, PrefConst.Key_Is_Registering_page, 5);


        loadLayout();

    }

    private void loadLayout() {

        rlt_next = (RelativeLayout)findViewById(R.id.ryt_next);
        rlt_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ContactActivity.this, ContactListActivity.class));
                finish();
                overridePendingTransition(0,0);
            }
        });


    }


    @Override
    public void onBackPressed() {

        startActivity(new Intent(this, UploadImageActivity.class));
        finish();

        overridePendingTransition(0,0);
    }
}
