package com.friendship.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.friendship.FriendShipApplication;
import com.friendship.R;
import com.friendship.base.CommonActivity;
import com.friendship.commons.Constants;
import com.friendship.commons.Urls;
import com.friendship.preference.PrefConst;
import com.friendship.preference.Preference;
import com.friendship.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class FirstNameActivity extends CommonActivity implements View.OnClickListener {

    EditText edt_firstName, edt_lastName;
    RelativeLayout rlt_next;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_name);
        Preference.getInstance().put(FirstNameActivity.this,PrefConst.Key_Is_Registering_page,1);
        loadLayout();
    }

    private void loadLayout() {

        edt_firstName = (EditText) findViewById(R.id.edt_firstname);
        edt_lastName = (EditText) findViewById(R.id.edt_lastname);
        edt_firstName.addTextChangedListener(new MyTextWatcher(edt_firstName));
        edt_lastName.addTextChangedListener(new MyTextWatcher(edt_lastName));
        rlt_next = (RelativeLayout) findViewById(R.id.ryt_next);
        rlt_next.setOnClickListener(this);

        if (Preference.getInstance().HasKey(FirstNameActivity.this, PrefConst.Key_firstName)){
            edt_firstName.setText(Preference.getInstance().getValue(FirstNameActivity.this, PrefConst.Key_firstName, PrefConst.Key_firstName));
            edt_lastName.setText(Preference.getInstance().getValue(FirstNameActivity.this, PrefConst.Key_lastName, PrefConst.Key_lastName));
        }

        validateTextField();

        LinearLayout lyt_container = (LinearLayout) findViewById(R.id.activity_first_name);
        lyt_container.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_firstName.getWindowToken(), 0);
                return false;
            }
        });
    }

    private void gotoMobileNumber() {
        if (Utils.isOnline(FirstNameActivity.this)) {
            doRegister();
        } else {
            showToast(getString(R.string.error_internet));
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.ryt_next:
                gotoMobileNumber();
                break;
        }

    }

    public void doRegister() {

        Preference.getInstance().put(FirstNameActivity.this, PrefConst.Key_firstName, edt_firstName.getText().toString());
        Preference.getInstance().put(FirstNameActivity.this, PrefConst.Key_lastName, edt_lastName.getText().toString());

        Intent MobileIntent = new Intent(FirstNameActivity.this, MobileNumberActivity.class);
        startActivity(MobileIntent);
        finish();

        overridePendingTransition(0, 0);
    }

    @Override
    public void onBackPressed() {
        onExit();
    }

    private class MyTextWatcher implements TextWatcher {
        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.edt_firstname:
                    validateTextField();
                    break;
                case R.id.edt_lastname:
                    validateTextField();
                    break;
            }
        }
    }

    private void validateTextField() {
        if (edt_firstName.getText().toString().trim().length() <= 0 || edt_lastName.getText().toString().trim().length() <= 0) {
            rlt_next.setEnabled(false);
        } else {
            rlt_next.setEnabled(true);
        }
    }
}
