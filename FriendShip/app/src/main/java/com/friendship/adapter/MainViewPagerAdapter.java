package com.friendship.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.KeyEvent;
import android.widget.TextView;

import com.friendship.R;
import com.friendship.activity.MainActivity;
import com.friendship.fragment.FriendsFragment;
import com.friendship.fragment.SearchFragment;
import com.friendship.fragment.ShipsFragment;
import com.friendship.fragment.StatusFragment;

/**
 * Created by HugeRain on 3/8/2017.
 */

public class MainViewPagerAdapter extends FragmentStatePagerAdapter {

    Context _context;

    public MainViewPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        _context = context;

    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;

        switch (position){

            case 0:

                fragment = new SearchFragment(_context);
                break;

            case 1:
                fragment = new StatusFragment(_context);
                break;

            case 2:

                fragment =new FriendsFragment(_context);
                break;

            case 3:
                fragment = new ShipsFragment(_context);
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        String title = "";

        switch (position){

            case 0:
                title = _context.getString(R.string.search);
                break;

            case 1:
                title = _context.getString(R.string.status);
                break;

            case 2:
                title = _context.getString(R.string.friends);
                break;

            case 3:
                title = _context.getString(R.string.ships);
        }

        return title;
    }

}
