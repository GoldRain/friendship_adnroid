package com.friendship.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.friendship.R;
import com.friendship.activity.ContactListActivity;
import com.friendship.model.FriendModel;
import com.friendship.utils.RadiusImageView;

import java.util.ArrayList;

/**
 * Created by HugeRain on 3/9/2017.
 */

public class AddContactListAdapter extends BaseAdapter {

    ContactListActivity _activity;
    ArrayList<FriendModel> _friends = new ArrayList<>();
    ArrayList<FriendModel> _allFriend = new ArrayList<>();
    ArrayList<FriendModel> _alllFriend = new ArrayList<>();

    TextView txv_number;

    int selectedIndex = 0;
    int selectedColor = Color.parseColor("#2f78f1");
    int clearColor = Color.parseColor("#00000000");
    int arraySize = 0;

    public AddContactListAdapter(ContactListActivity activity) {

        _activity = activity;
        selectedIndex = -1;

    }

    public void setFriends(ArrayList<FriendModel> friends) {

        _allFriend.addAll(friends);
        _friends.clear();
        _friends = _allFriend;
        _alllFriend = friends;
        int valuee = _alllFriend.size();
        Log.d("ddddddddd", String.valueOf(valuee));
        notifyDataSetChanged();
    }


    public void setSelectedIndex(int index) {

        selectedIndex = index;
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return _friends.size();
    }

    @Override
    public Object getItem(int position) {
        return _friends.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ListHolder listHolder;

        if (convertView == null) {
            listHolder = new ListHolder();

            LayoutInflater inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.contact_list_item, parent, false);

            listHolder.imv_photo = (RadiusImageView) convertView.findViewById(R.id.imv_photo_item);
            listHolder.txv_name = (TextView) convertView.findViewById(R.id.txv_name_con);
            listHolder.txv_description = (TextView) convertView.findViewById(R.id.txv_description);
            listHolder.imv_fs = (ImageView) convertView.findViewById(R.id.imv_fs);
            listHolder.imv_plus = (ImageView) convertView.findViewById(R.id.imv_plus);

            listHolder.imv_plus.setSelected(true);
            //listHolder.imv_fs.setSelected(false);
            convertView.setTag(listHolder);

        } else {

            listHolder = (ListHolder) convertView.getTag();
        }

        final FriendModel friendModel = (FriendModel) _friends.get(position);

        if (friendModel.getFromPhone().matches("true")) {
            listHolder.imv_fs.setSelected(false);
        } else {
            listHolder.imv_fs.setSelected(true);
        }


        if (friendModel.getPlusminus().matches("plus")) {
            listHolder.imv_plus.setSelected(true);
        } else {
            listHolder.imv_plus.setSelected(false);
        }
     //
      if (isValidEmail(friendModel.getEmail() )) {
            listHolder.txv_name.setText(friendModel.getEmail());
        } else {
          listHolder.txv_name.setText(friendModel.get_name());
        }

        //listHolder.txv_name.setText(friendModel.get_name());
        listHolder.txv_description.setText(friendModel.get_description());

        Glide.with(_activity).load(friendModel.get_photo_url()).placeholder(R.drawable.bg_non_profile).into(listHolder.imv_photo);

        if (selectedIndex != -1 && position == selectedIndex) {

            convertView.setBackgroundResource(R.drawable.list_selected);

            listHolder.txv_name.setTextColor(_activity.getResources().getColor(R.color.white));
            listHolder.txv_description.setTextColor(_activity.getResources().getColor(R.color.white));

            // listHolder.imv_fs.setImageResource(R.drawable.fs_white);
            // listHolder.imv_plus.setImageResource(R.drawable.minus);


        } else {

            convertView.setBackgroundResource(R.drawable.list_select);

            listHolder.txv_name.setTextColor(_activity.getResources().getColor(R.color.black));
            listHolder.txv_description.setTextColor(_activity.getResources().getColor(R.color.grey_line));
            //  listHolder.imv_fs.setImageResource(R.drawable.fs_blue);
            //  listHolder.imv_plus.setImageResource(R.drawable.plus);
        }

        listHolder.imv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //_activity.showToast("remove" + position);

                if (listHolder.imv_plus.isSelected()) {
                    addItem(friendModel);
                    listHolder.imv_plus.setSelected(false);
                    friendModel.setPlusminus("minus");
                } else {
                    friendModel.setPlusminus("plus");
                    removeItem(friendModel);
                    listHolder.imv_plus.setSelected(true);
                }


            }
        });

  /*      listHolder.imv_fs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (v.isSelected()) {

                    v.setSelected(false);

                } else v.setSelected(true);


            }
        });
*/
        return convertView;
    }
    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public void filter(String charText) {

        charText = charText.toLowerCase();

        _friends.clear();

        if (charText.length() == 0) {

            _friends.addAll(_alllFriend);

        } else {


            for (int i = 0; i < _alllFriend.size(); i++) {

                FriendModel user = _alllFriend.get(i);

                String value = user.get_name().toLowerCase();

                if (value.contains(charText)) {

                    _friends.add(user);
                }
            }
        }
        txv_number = (TextView) _activity.findViewById(R.id.txv_number);

       // txv_number.setText(0 + "");

        notifyDataSetChanged();
    }

    public void removeItem(FriendModel model) {
        txv_number = (TextView) _activity.findViewById(R.id.txv_number);
        //_friends.remove(model);
        arraySize = arraySize - 1;
        txv_number.setText(arraySize + "");
        notifyDataSetChanged();


    }

    public void addItem(FriendModel model) {
        txv_number = (TextView) _activity.findViewById(R.id.txv_number);

        //   _friends.add(model);
        arraySize = arraySize + 1;

        txv_number.setText(arraySize + "");


        notifyDataSetChanged();


    }


    public class ListHolder {

        RadiusImageView imv_photo;
        TextView txv_name;
        TextView txv_description;
        ImageView imv_fs;
        ImageView imv_plus;

    }
}
