package com.friendship.commons;

/**
 * Created by HugeRain on 3/8/2017.
 */

public class Constants {

    public static final int VOLLEY_TIME_OUT = 6000;
    public static final int SPLASH_TIME = 2500;
    public static final int PROFILE_IMAGE_SIZE = 256;

    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;

    public static final int PERMISSION = 105;
    public static final int PERMISSION2 = 101;

    public static final String Authorization = "Basic YmJBcHBBbm9ueW1vdXM6a3pjYjZ5eGtsbzc4OTlidTd2";
    public static final String Content_Type = "application/json";

}
//; charset=UTF-8
