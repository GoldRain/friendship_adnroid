package com.friendship.commons;

/**
 * Created by HugeRain on 3/8/2017.
 */

public class Urls {

    public static final String Get_Access_Token = "https://bbAppAnonymous:kzcb6yxklo7899bu7v@api.boatbook.info/oauth/token?grant_type=client_credentials";
    public static final String Register = "https://api.boatbook.info/api/register";
    public static final String Send_Msg_Code = "https://api.boatbook.info/api/activate?";
    public static final String Login = "https://api.boatbook.info/oauth/token";
    public static final String Send_Image = "https://api.boatbook.info/api/account";
    public static final String Receive_FriendList = "https://api.boatbook.info/api/friends/potentialFriends";
    public static final String Send_Contact = "https://api.boatbook.info/api/friends/inviteFriends";

}
